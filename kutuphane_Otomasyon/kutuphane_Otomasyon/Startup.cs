﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kutuphane_Otomasyon.Startup))]
namespace kutuphane_Otomasyon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
