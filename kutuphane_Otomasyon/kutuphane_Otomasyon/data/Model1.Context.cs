﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace kutuphane_Otomasyon.data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class kutuphane_DbEntities : DbContext
    {
        public kutuphane_DbEntities()
            : base("name=kutuphane_DbEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<kitaplar> kitaplars { get; set; }
        public DbSet<kullanicilar> kullanicilars { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
